package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A bucket (keyword) from Bold.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bucket {

    /**
     * The hub the bucket belongs to.
     */
    public Hub hub;

    /**
     * The keyword for the bucket (optional).
     */
    public String keyword;

    /**
     * Whether the bucket is a fallback bucket or not (in which case the keyword will be blank or null).
     */
    public boolean fallback;

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isFallback() {
        return fallback;
    }

    public void setFallback(boolean fallback) {
        this.fallback = fallback;
    }

    @Override
    public String toString() {
        return String.format("<Bucket: hub=%s, keyword=%s, fallback=%b>", hub, keyword, fallback);
    }

}
