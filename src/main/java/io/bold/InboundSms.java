package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * An inbound SMS, useful for actions invoked through a webhook.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InboundSms {

    /**
     * Id of the message.
     */
    public long id;

    /**
     * Sender of the message.
     */
    public Contact sender;

    /**
     * Hub that received the message.
     */
    public Hub hub;

    /**
     * Bucket that received the message.
     */
    public Bucket bucket;

    /**
     * Full content of the message.
     */
    public String content;

    /**
     * Processed content of the message (message without the keyword).
     */
    @JsonProperty("processed_content")
    public String processedContent;

    /**
     * The keyword; should match the keyword of the bucket.
     */
    public String keyword;

    /**
     * Date the message was received.
     */
    public Date received;

    /**
     * Date the message was processed.
     */
    public Date processed;

    /**
     * Link to send a reply--does not include the hostname.
     */
    @JsonProperty("link_reply")
    public String replyLink;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Contact getSender() {
        return sender;
    }

    public void setSender(Contact sender) {
        this.sender = sender;
    }

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public Bucket getBucket() {
        return bucket;
    }

    public void setBucket(Bucket bucket) {
        this.bucket = bucket;
    }

    public String getProcessedContent() {
        return processedContent;
    }

    public void setProcessedContent(String processedContent) {
        this.processedContent = processedContent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public Date getProcessed() {
        return processed;
    }

    public void setProcessed(Date processed) {
        this.processed = processed;
    }

    public String getReplyLink() {
        return replyLink;
    }

    public void setReplyLink(String replyLink) {
        this.replyLink = replyLink;
    }

    @Override
    public String toString() {
        return String.format("<InboundSms: id=%d, hub=%s, bucket=%s, sender=%s, content=%s, processedContent=%s, received=%s, processed=%s, replyLink=%s>", id, hub, bucket, sender, content, processedContent, received, processed, replyLink);
    }
}
