package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A contact registered/to register in Bold.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {

    /**
     * The id of the contact.
     */
    private Long id;

    /**
     * The name of the contact.
     */
    private String name;

    /**
     * The number of the contact, in E.164 format.
     */
    private String number;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("<Contact: id=%d, name=%s, number=%s>", id, name, number);
    }
}
