package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A hub from Bold.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Hub {

    /**
     * The hub's id.
     */
    private Long id;

    /**
     * The name of the hub.
     */
    private String name;

    /**
     * The number of the hub.
     */
    @JsonProperty("nr")
    private String number;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("<Hub: id=%s, name=%s, number=%s>", id, name, number);
    }
}
