package io.bold;

import com.google.i18n.phonenumbers.Phonenumber;

import java.io.IOException;
import java.util.List;

/**
 * Client interface for Bold.io's SMS hub.
 */
public interface Client {

    /**
     * Find the subscriptions for the number.
     *
     * @param number number in E.164 format
     * @return the list of subscriptions
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    List<Subscription> findSubscriptions(String number) throws IOException, BoldException;

    /**
     * Finds the subscriptions for the number.
     *
     * @param number number
     * @return the list of subscriptions
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    List<Subscription> findSubscriptions(Phonenumber.PhoneNumber number) throws IOException, BoldException;

    /**
     * Subscribe the number to the subscription.
     *
     * @param number           number to subscribe (E.164 format)
     * @param subscriptionName subscription to subscribe to
     * @return <code>false</code> in case the number was already subscribed
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    boolean subscribe(String number, String subscriptionName) throws IOException, BoldException;

    /**
     * Subscribe the number to the subscription.
     *
     * @param number           number
     * @param subscriptionName subscription to subscribe to
     * @return <code>false</code> in case the number was already subscribed
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    boolean subscribe(Phonenumber.PhoneNumber number, String subscriptionName) throws IOException, BoldException;

    /**
     * Unsubscribe the number.
     *
     * @param number           number to unsubscribe (E.164 format)
     * @param subscriptionName name of the subscription
     * @return <code>false</code> if the number was not subscribed in the first place
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    boolean unsubscribe(String number, String subscriptionName) throws IOException, BoldException;

    /**
     * Unsubscribe the number.
     *
     * @param number           number to unsubscribe
     * @param subscriptionName name of the subscription
     * @return <code>false</code> if the number was not subscribed in the first place
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    boolean unsubscribe(Phonenumber.PhoneNumber number, String subscriptionName) throws IOException, BoldException;

    /**
     * Send a message.
     *
     * @param hubId                    id of the hub
     * @param number                   number to send to
     * @param content                  content to send
     * @param requiredSubscriptionName name of the subscription required to send this message (optional)
     * @return the outbound sms or null in case the message could not be sent (not subscribed, blocked, ...)
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    OutboundSms send(String hubId, String number, String content, String requiredSubscriptionName) throws IOException, BoldException;

    /**
     * Send a message.
     *
     * @param hubId                    id of the hub
     * @param number                   number to send to
     * @param content                  content to send
     * @param requiredSubscriptionName name of the subscription required to send this message (optional)
     * @return the outbound sms or null in case the message could not be sent (not subscribed, blocked, ...)
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    OutboundSms send(String hubId, Phonenumber.PhoneNumber number, String content, String requiredSubscriptionName) throws IOException, BoldException;

    /**
     * Send a message.
     *
     * @param hubId                    id of the hub
     * @param contactId                id of the contact to send to
     * @param content                  content to send
     * @param requiredSubscriptionName name of the subscription required to send this message (optional)
     * @return the outbound sms or null in case the message could not be sent (not subscribed, blocked, ...)
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    OutboundSms send(String hubId, long contactId, String content, String requiredSubscriptionName) throws IOException, BoldException;

    /**
     * Reply to a message.
     *
     * @param messageId id of the message to reply to
     * @param content   content to send
     * @return the outbound sms or null when the original message does not exist
     * @throws IOException   when something goes wrong contact the Bold app
     * @throws BoldException when something is wrong with the request
     */
    OutboundSms reply(long messageId, String content) throws IOException, BoldException;

    class BoldException extends RuntimeException {

        public BoldException() {
            super();
        }

        public BoldException(String msg) {
            super(msg);
        }

        public BoldException(Throwable t) {
            super(t);
        }

        public BoldException(String msg, Throwable t) {
            super(msg, t);
        }
    }


}
