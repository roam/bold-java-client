package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An outbound SMS message.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutboundSms {

    /**
     * Id of the sms.
     */
    private Long id;

    /**
     * The hub the SMS should be/is sent through.
     */
    private Hub hub;

    /**
     * The destination contact.
     */
    private Contact destination;

    /**
     * The content of the message.
     */
    private String content;

    /**
     * When the message was read.
     */
    @JsonProperty("read")
    private Long readTimestamp;

    /**
     * The status of the message.
     */
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public Contact getDestination() {
        return destination;
    }

    public void setDestination(Contact destination) {
        this.destination = destination;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getReadTimestamp() {
        return readTimestamp;
    }

    public void setReadTimestamp(Long readTimestamp) {
        this.readTimestamp = readTimestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("<OutboundSms: id=%d, hub=%s, destination=%s, content=%s, status=%s>", id, hub, destination, content, status);
    }
}
