package io.bold;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A subscription for a specific contact.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subscription {

    /**
     * Name of the subscription.
     */
    private String name;

    /**
     * The contact that is subscribed to this subscription.
     */
    private Contact contact;

    public Subscription() {
        super();
    }

    public Subscription(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return String.format("<Subscription: name=%s, contact=%s>", name, contact);
    }
}
