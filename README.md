# Bold.io Java client

This is the official Java client of Bold.io's SMS management application. You
can either download the jar from the source's `bin` directory or clone this
repository and build the client yourself. You can find the dependencies in the
included POM file (Google's libphonenumber and Guava and Jackson).

## Usage

First of all, when specifying a phone number as a `String`, you should opt for
the [E.164 format](http://en.wikipedia.org/wiki/E.164).

- Bad: `32475001122`
- Bad: `0475001122`
- Bad: `0032475001122`
- Good: `+32475001122`

Prefer the methods with the `PhoneNumber` arguments.

### Construct a client

    String endpoint = "http://localhost:8000";
    String user = "yourusername";
    String pwd = "yourpassword";
    Client client = new DefaultClient(endpoint, user, pwd);
    String number = "+32475001122";

### Working with subscriptions

When working with a number not yet registered with a contact in Bold or simply
a number without subscriptions, calling `findSubscriptions` will return an
empty list:

    List<Subscriptions> subscriptions = client.findSubscriptions(number);
    assertTrue(subscriptions.isEmpty());

After subscribing that changes:

    boolean subscribed = client.subscribe(number, "INFO");
    if (subscribed) {
        System.out.println(number + " is now subscribed");
    } else {
        System.out.println(number + " was already subscribed");
    }
    List<Subscriptions> subscriptions = client.findSubscriptions(number);
    assertEquals(1, subscriptions.size());

Of course, unsubscribing changes that again:

    boolean unsubscribed = client.unsubscribe(number, "INFO");
    if (unsubscribed) {
        System.out.println(number + " is now unsubscribed");
    } else {
        System.out.println(number + " wasn't subscribed");
    }
    List<Subscriptions> subscriptions = client.findSubscriptions(number);
    assertTrue(subscriptions.isEmpty());

### Sending a message

Sending a message requires you to know the id of the hub (displayed on the
hub's detail page).

    String hubId = "5";
    String content = "Hey there!";
    OutboundSms sms = client.send(hubId, number, content, null);

This will send the message to the given number through the given hub and
provide you with the registered information about that message. If you
only want to send the message when the number is subscribed to for example
`CATFACTS`, do this instead:

    OutboundSms sms = client.send(hubId, number, content, "CATFACTS");

### Replying to a message

Replying to a message is even easier:

    long messageId = 123L;
    OutboundSms reply = client.reply(messageId, "Thanks for the message!");


## Extras

This repo includes a demo server for handling webhook actions from Bold. Run
`demo/java/io/bold/WebhookHandler` and register its URL (default
`http://localhost:9000/`) as the URL of a webhook action in a bucket of
choice and you'll see inbound messages being logged to standard out.

## Questions? Bugs?

Please contact kevin@roam.be.